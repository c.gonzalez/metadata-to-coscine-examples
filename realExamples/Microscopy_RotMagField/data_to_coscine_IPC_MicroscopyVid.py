###############################################################################
# IPC Microscopy Video Data Transfer to Coscine
###############################################################################
# Copyright (c) 2022 RWTH Aachen University
# Public Domain MIT License
# Authors: N. Parks, D. Braunmiller | SFB985
###############################################################################
"""
This script is a practical example used to transfer microscopy video data
from a computer to a coscine S3 resource and is in use at the RWTH IPC.
It extracts metadata from a file path and uploads both the data
and metadata to Coscine.
It assumes the following file path structure:
D:\Dominik\PBox-MagSet\[productionMethod]\[sampleName]\[measurmentSeries]\
    Rohfiles\[fieldStrength]mT-[rotationRate]gps-[frameRate]fps-[rotation]°
    -[datetime]-[4-digitNumber].avi
where the bracketed values are variables (brackets should not be included 
in the actual file name).

This version uses the S3 protocol to transfer the data (requires S3 resource)
and the REST API to transfer the metadata.
"""
###############################################################################
# Dependencies
###############################################################################
import logging
from logging.handlers import TimedRotatingFileHandler
import os
import sys
import json
import re as regex
from pathlib import Path
from datetime import datetime
from tkinter import Y
import coscine
###############################################################################
# Setup
###############################################################################
# load the config file
config_path = Path('.') / Path('config.json')
with open(config_path, "rt", encoding="utf-8") as fp:
    cfg = json.load(fp)
###############################################################################
logger = logging.getLogger("coscine-upload")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s — %(name)s — %(levelname)s — %(message)s"
)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)
file_handler = TimedRotatingFileHandler(cfg["log_file_name"], when='midnight')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
###############################################################################
# Functions
###############################################################################
def upload_file(resource: coscine.Resource, path: Path) -> bool:
    """
    Uploads a file to a Coscine resource
    Returns
    -------
    True
        If the file was uploaded
    False
        If the file was not uploaded e.g. when it already exists
    """
    if not resource.exists(path.name):
        resource.s3.upload(path.name, str(path))
    else:
        ask_overwrite = input(f"File '{path.name}' already exists at "
                                    "destination. Overwrite? (Y/n)") 
        if ask_overwrite.upper() == "Y":
            resource.s3.upload(path.name, str(path))
        else:
            logger.info(f"Skipping file '{path.name}'...")
            return False
    return True
###############################################################################
def parse_filename(path: Path) -> dict:
    """
    Parse information stored in the filename and return it as a dict
    """
    data: dict = {}
    # get a list of the single elements of the path to fill in metadata
    ls_filename = path.name.replace(',', '.')
    ls_filename = regex.split('_|-', ls_filename)
    # fill in metadata from path
    data['Production method'] = path.parts[3] 
    data['Sample ID'] = path.parts[4]
    data['Measurement Series'] = path.parts[5]

    for i in ls_filename:
        for word in ['mT','gps','fps','°']:
            word_pos = i.find(word)
            if word_pos != -1:
                data[word] = float(i[0:word_pos])
    if not data["fps"]:
        data["fps"] = data["gps"]
    return normalize_parsed_data(data)
###############################################################################
def normalize_parsed_data(data: dict) -> dict:
    MAPPING: dict = {
        "mT": "Field strength [mT]",
        "gps": "Rotation rate [gps]",
        "fps": "Frame rate [fps]",
        "°": "Rotation [°]",
    }
        
    for key, value in MAPPING.items():
        if key in data:
            data[value] = data[key]
            del data[key]
    return data
###############################################################################
def upload_metadata(resource: coscine.Resource, path: Path):
    # Metadata forms are cached, so this is not too expensive
    # to call for each file
    metadata = resource.metadata_form()
    # default values 
    metadata['Creator'] = 'Dominik Braunmiller'
    metadata['Solvent'] = 'water'
    metadata['Camera'] = 'FKIR-BFS-U3-16S2M-CS'
    metadata['Microscope'] = 'Zeiss Axioskope'
    metadata['Objective lens'] = '5x Zeiss Achrostigmat'
    ti_c = os.path.getctime(path.parent)
    metadata['Date and time of measurement'] = datetime.fromtimestamp(ti_c)
    # Extract metadata from file name
    for key, value in parse_filename(path).items():
        metadata[key] = value
    file = resource.object(path.name)
    file.update(metadata)
    logger.info(f"Uploaded metadata for {path.name}")
    
###############################################################################
def main():
    client_settings = coscine.Settings(verbose=False)
    client = coscine.Client(cfg["token_cos"], settings=client_settings)
    resource = client.project(cfg["cos_project"],
        toplevel=False).resource(cfg["cos_resource"])
    # go through the data files locally
    # if a file isn't in Coscine, upload it to Coscine
    for path in [x for x in Path(cfg["data_folder"]).rglob("*") if x.is_file]:
        if upload_file(resource, path):
            upload_metadata(resource, path)
            
###############################################################################
# Main
###############################################################################
try:
    main()
except Exception as exp:
    # writes error info to log file if there is an exception
    logger.exception(exp)
    # popup message if there is an error
    # ctypes.windll.user32.MessageBoxW(0,
    #     "There was an error! Please check logs!",
    #                     "Coscine Data Upload", 1)
###############################################################################