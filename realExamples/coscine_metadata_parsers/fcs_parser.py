"""
Imports needed for FCSParser class to parse file and fill metadata form
"""
import re
from datetime import datetime, date
from pathlib import Path
from collections import Counter
from coscine import MetadataForm
import flowkit as fk


from ..iparser import IParser


class FCSParser(IParser):
    """
    This class accepts IParser and contains functions
    to parse file and fill metadata form.
    """
    def __init__(self):
        super().__init__()
        self.adjacent_files = []

    def parse_file(self, file_name: str, adjacent_files: list[str] = None) -> dict:
        # Use flowkit to parse the file and extract metadata
        if adjacent_files is None:
            adjacent_files = []
        self.adjacent_files = adjacent_files
        self.parsed_metadata.update(fk.Sample(file_name).get_metadata())
        markers_analysed = self.get_sample_markers(self.parsed_metadata)

        # These Keys need to be updated to match the Coscine MetadataForm
        self.parsed_metadata.update(
            {
                "Hypothesis": self.get_experiment_name(),
                "Instrument": self.get_instrument_name(self.parsed_metadata["cyt"]),
                "Number of Samples": self.get_number_of_sample_by_exp_name(
                    self.adjacent_files
                ),
                "Date of Analysis": self.convert_date_string(
                    self.parsed_metadata["date"]
                ),
                "Markers analysed": markers_analysed,
            }
        )
        return self.parsed_metadata

    def clean_metadata(self, metadata_form: MetadataForm)-> dict:
        """
        Takes the parsed metadata from the file and strips out keys that are not in the Coscine MetadataForm
        Returns: 
            dict: A dictionary that contains metadata keys that match the MetadataForm
        """
        cleaned_metadata = {}
        for key, value in self.parsed_metadata.items():
            if key in metadata_form.keys():
                cleaned_metadata[key] = value
        return cleaned_metadata

    def fill_metadata_form(self, metadata_form) -> MetadataForm:
        """
        Given a MetadataForm, create a new MetadataForm with the metadata that we have parsed from
        the file

        Args:
            metadata_form (MetadataForm): The MetadataForm to fill. This is provided by Coscine.

        Returns:
            MetadataForm: The MetadataForm with the metadata that we have parsed from the file
        """
        self.parsed_metadata = self.clean_metadata(metadata_form)
        return super().fill_metadata_form(metadata_form)


    def get_experiment_name(self) -> str:
        """
        Check the extracted metadata for an experiment name and return it.

        Args:
            raw_metadata (dict): The extracted metadata

        Returns:
            str: The experiment name

        Raises:
            ValueError: If raw_metadata is not a dictionary
            KeyError: If no experiment name is found in the extracted metadata
        """
        experiment_name_keys = [
            "experiment name",
            "groupname",
            "experiment_name",
            "@testname",
            "Hypothesis",
        ]

        for key, value in self.parsed_metadata.items():
            for experiment_name_key in experiment_name_keys:
                if experiment_name_key == key:
                    return value

        raise KeyError("No experiment name found in metadata.")

    def get_number_of_sample_by_exp_name(self, adjacent_files: list) -> int:
        """
        This function examines the metadata from the file
        to count the number of samples based on the experiment name.

        Args:
            adjacent_files (list): A list of adjacent files to
            consider for counting experiment names.

        Returns:
            int: integer indicating number of samples based on experiment name
        """
        # If no adjacent files then number of samples returned is 1
        if len(adjacent_files) == 0:
            return 1

        # Create a list to store the experiment names from adjacent files
        experiment_names = []

        # Iterate over the adjacent files
        for adjacent_file in adjacent_files:
            # For each file, create a new parser and parse the file
            if self.file_name == adjacent_file:
                continue
            parser = FCSParser()
            parser.parse_file(adjacent_file)

            # Get the experiment name from the parsed file metadata
            experiment_name = parser.get_experiment_name()

            # Append the experiment name to the list
            experiment_names.append(experiment_name)

        # Use Counter to count occurrences of each experiment name
        experiment_name_counts = Counter(experiment_names)

        total_count_of_experiment_names = experiment_name_counts[self.get_experiment_name()]+1

        # Return the count of occurrences for the experiment
        # name that matches the current file
        return total_count_of_experiment_names

    def get_sample_markers(self, raw_metadata: dict) -> list:
        """
        This Function uses Regex to extract values from metadata keys
        that begin with p followed by a number
        and returns a list of sample markers that are of interest.
        Arg:
            self: user instance
            metadata: metadata from the file
        Returns
            list of values based on the metadata key pattern
        """
        pattern = re.compile(r"^p\d+s$")
        markers = []

        markers += [raw_metadata[key] for key in raw_metadata if pattern.match(key)]
        if len(markers) == 0:
            markers.append("None")
        return markers

    def get_instrument_name(self, cyt: str) -> str:
        """
        This function converts the instrument name from the
        fcs/lmd file to the name that is used in Coscine

        Arg:
            cyt: key in the fcs/lmd files that represents instrument name

        Returns:
            Name of instrument as it is written in Coscine
        """
        if not isinstance(cyt, str):
            raise ValueError

        if cyt == "Aurora":
            return "Cytek Aurora"
        if cyt == "FACSAriaII":
            return "BD Aria"
        if cyt == "LSRFortessa":
            return "BD LSRFortessa"
        return "andere"

    def convert_date_string(self, date_string) -> datetime:
        """
        This extracts value from metadata key "date"
        and converts it from string format to datetime.datetime

        Arg:
            date_string: date in string form that will need to be changed

        Returns:
            Date in datetime.datetime format

        Raises:
            ValueError: If no format is found for the date_string
        """
        potential_date_formats = ["%d-%b-%Y", "%d-%b-%y"]

        for date_format in potential_date_formats:
            try:
                parsed_date = datetime.strptime(date_string, date_format)
                return parsed_date
            except ValueError:
                pass

        raise ValueError(f"Unknown Date Format: {date_string}")

    def update_file_name(self, file_name: str) -> str:
        """
       This function changes file name format to be accepted by coscine

        Arg:
           file name: string form that will need to be changed

        Returns:
            file name with \\

        Raises:
            ValueError: If no format is found for the date_string
        """

        if self:
            today = date.today().strftime("%Y-%m-%d")
            file_stem = Path(file_name).stem  # Get the filename without extension
            file_extension = Path(file_name).suffix  # Get the file extension
        return f"{file_stem}_{today}{file_extension}"