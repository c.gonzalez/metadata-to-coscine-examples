# Upload the HiBC data on Coscine
# 2023-01-19
# Charlie Pauvert

import coscine
import pandas as pd
import os

with open("token.txt", "rt") as f:
    token = f.read().strip()

# Connect to the client
client = coscine.Client(token)
# Access the project
prj = client.project("AG_CLAVEL_HiBC")

#
#  Create a Resource for the genomes
#
form = client.resource_form()  # need coscine > 0.8
# Document the resource metadata
resource_metadata = {
    "Resource Name": "genomes",
    "Display Name": "genomes",
    "Resource Type": "rdss3rwth",
    "Resource Size": 1,
    "Resource Description": "The genomes of the HiBC collection in gzipped FASTA format. For the moment, the plasmids sequences are not included.",
    "Discipline": "Microbiology, Virology and Immunology 204",
    "Metadata Visibility": "Public",
    "Application Profile": "fasta",
}

# Fill in the form
form.fill(resource_metadata)

# Upload and create the Resource
# prj.create_resource(form)

# The Resource being already created it is just loaded
genomes = prj.resource("genomes")
# Fetch the Application profile and metadata form
new_fasta = genomes.metadata_form()
#
# print(new_fasta)
#
#  +---+------------------------------------------------+-------+
#  | C | Property                                       | Value |
#  +---+------------------------------------------------+-------+
#  |   | FASTQ accession*                               |       |
#  |   | Marker for taxonomic identification*           |       |
#  | X | Assembly Quality*                              |       |
#  |   | Assembly Software*                             |       |
#  |   | Coverage*                                      |       |
#  |   | Number of Contigs*                             |       |
#  |   | N50                                            |       |
#  |   | Large ribosomal Sub-Unit (e.g. 23S) recovered* |       |
#  |   | LSU Recover Software*                          |       |
#  |   | Small ribosomal Sub-Unit (e.g. 16S) recovered* |       |
#  |   | SSU Recover Software*                          |       |
#  |   | Number of tRNAs*                               |       |
#  |   | tRNAs Extraction Software*                     |       |
#  |   | Completeness*                                  |       |
#  |   | Completeness Software*                         |       |
#  |   | Contamination*                                 |       |
#  +---+------------------------------------------------+-------+


# Assembly Quality is somewhat controlled but not with a dedicated vocabulary that mapped to PIDs
# this is "only" a selection of entries
new_fasta.has_vocabulary("Assembly Quality")  # False
new_fasta.has_selection("Assembly Quality")  # True
form.selection("Assembly Quality")
# ['Finished genome', 'High-quality draft genome', 'Medium-quality draft genome', 'Low-quality draft genome', 'Genome fragment(s)']

#
# Helpers functions
#

def assign_genome_quality(dict_genome_metadata):
    if "High-quality" in dict_genome_metadata["assembly_qual"]:
        qual = "High-quality draft genome"
    elif (
        dict_genome_metadata["compl_score"] >= 50
        and dict_genome_metadata["contam_score"] <= 10
    ):
        qual = "Medium-quality draft genome"
    elif (
        dict_genome_metadata["compl_score"] < 50
        and dict_genome_metadata["contam_score"] <= 10
    ):
        qual = "Low-quality draft genome"
    else:
        qual = "Genome fragment(s)"
    return qual


def get_metadata_from_df(row):
    metadata_dict = {
        "FASTQ accession": row["forward_file"],
        "Marker for taxonomic identification": "16S rRNA gene",
        "Assembly Quality": assign_genome_quality(row),
        "Assembly Software": row["assembly_software"],
        "Coverage": float(row["coverage"]),
        "Number of Contigs": row["number_contig"],
        "N50": float(row["N50"]),
        "Large ribosomal Sub-Unit (e.g. 23S) recovered": row["is_LSU_grtr_0"],
        "LSU Recover Software": row["LSU_recover_software"],
        "Small ribosomal Sub-Unit (e.g. 16S) recovered": row["is_SSU_grtr_0"],
        "SSU Recover Software": row["SSU_recover_software"],
        "Number of tRNAs": row["trnas"],
        "tRNAs Extraction Software": row["trna_ext_software"],
        "Completeness": row["compl_score"],
        "Completeness Software": row["compl_software"],
        "Contamination": row["compl_score"],
    }
    return metadata_dict


def find_the_genome_file(
    dict_genome_metadata, prefix="/home/cpauvert/projects/bacterial-collection/hibc-genomes"
):
    genome = os.path.basename(dict_genome_metadata["archive_file"])
    if (
        dict_genome_metadata["compl_score"] > 90
        and dict_genome_metadata["contam_score"] < 5
    ):
        path = f"{prefix}/genomes-plasmids/{genome}"
    else:
        path = f"{prefix}/hibc-genomes-with-contamination/genomes-plasmids/{genome}"
    return path


#
# Read in metadata table
#
hibc = pd.read_csv(
    "/home/cpauvert/projects/bacterial-collection/hibc-genomes/20221216_hibc-genome-quality-summary.csv"
)

# Upload strategy to coscine
# from the dataframe of genomes
# convert all rows as dictionary and convert the dataframe as a list
hibc_records = hibc.to_dict(orient="records")

for record in hibc_records[2:]:
    # Instantiate a new Metadata form with Coscine SDK method
    new_fasta = genomes.metadata_form()
    # Extract the medata from the record with a custom helper function
    mdata = get_metadata_from_df(record)
    # Fill the form with the metadata dictionary with Coscine SDK method
    new_fasta.fill(mdata)
    # Extract the path to the genome file with a custom helper function
    path_genome = find_the_genome_file(record)
    # Upload on Coscine witha SDK method expecting
    #  name of the object, path and Coscine SDK object with the filled metadata form
    genomes.upload(os.path.basename(path_genome), path_genome, new_fasta)



